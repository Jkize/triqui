import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { RoomService } from './room.service';

/**
 * Service General
 */
@Injectable({
  providedIn: 'root'
})
export class ServicesGService {

  constructor(public AuthS:AuthService, public RoomS:RoomService) { }
}
