
export interface AuthI{
    id:string 
}

export interface PlayerI{
    id:string;
    name:string;
}


export interface GameI{
    isFinished:boolean, //->1 player A, 2-> playerB
    board:any,
    movements:number;
    win?:number;
}

export interface RoomI{
    id:string;
    playerA:PlayerI;
    playerB:PlayerI;
    cont:number; // par arranca playerA, impar arranca PlayerB
    game:GameI
}

export interface UpdateNamePlayerRequestI{
    idRoom:string;
    isOwn:boolean; 
    player:PlayerI
}