import { RoomI, UpdateNamePlayerRequestI } from "./models.interface";

export interface ServiceResponse {
    successful(data: any): void;
    failure(mss: string): void;
}

export interface RoomServiceI {

    newGame(room: RoomI, successful: any, failure: any):void;
    updateNamePlayer(data: UpdateNamePlayerRequestI, successful: any, failure: any):void;

    restartingGame(room: RoomI, successful: any, failure: any): void
    updateGame(room: RoomI, successful:any, failure:any):void;
}