import { ServicesGService } from 'src/app/2.services/services-g.service';
import { RoomI, PlayerI, GameI } from './../../3.interfaces/models.interface';
import { GameComponent } from './game.component';


export class ControllerGameComponent {

  constructor(private c: GameComponent, private sG: ServicesGService) {
  }



  /**
   * Get a board matrix base the board of room 
   * @param room 
   * @returns 
   */
  getBoard(room: RoomI): number[][] {

    let array: number[][] = []
    let i = 0;
    for (const key in room.game.board) {
      array.push([]);
      for (const dt of (room.game.board[key] as Array<number>)) {
        array[i].push(dt);
      }
      i++;
    }
    return array;

  }

  /**
   * Check if is my turn
   * @param isOwn 
   * @param room 
   */
  isMyTurn(isOwn: boolean, room: RoomI): boolean {
    if (isOwn) {
      if (room.cont % 2 == 0) {
        return room.game.movements % 2 == 0;
      } else {
        return room.game.movements % 2 == 1;
      }
    } else {
      if (room.cont % 2 == 1) {
        return room.game.movements % 2 == 0;
      } else {
        return room.game.movements % 2 == 1;
      }
    }
  }

  /**
   * Update the name of the player
   * @param isOwn 
   * @param name 
   * @param room 
   */
  updateName(isOwn: boolean, player: PlayerI, room: RoomI) {
    this.sG.RoomS.updateNamePlayer({
      idRoom: room.id,
      player: player,
      isOwn: isOwn
    }, () => {

    }, () => {
      alert("Ocurrió un error al actualizar el nombre del jugador")
    })
  }

/**
 * Restar the game 
 * @param isOwn 
 * @param room 
 * @returns 
 */
  restarGame(  room: RoomI) {

  

    let roomCopy = JSON.parse(JSON.stringify(room));

    let game: GameI = {
      board: { 0: [0, 0, 0], 1: [0, 0, 0], 2: [0, 0, 0] },
      isFinished: false,
      movements: 0,
      win: -1
    }

    roomCopy.game = game;
    roomCopy.cont += 1;

    this.sG.RoomS.restartingGame(roomCopy, () => {

    },
      () => {
        alert("No se pudo restaurar el juego :'(")
      })


  }

  continueClickCell(isOwn: boolean, i: number, j: number, room: RoomI, boardAux: number[][]) {
    //is own -> player A is 1 otherwise player B is 2

    let player = (isOwn) ? 1 : 2;
    boardAux[i][j] = player;
    room.game.board[i + ""][j] = player;
    room.game.movements += 1;

    let win = this.whoWinnerFinished(boardAux, room);

    if (win != -1) {
      room.game.isFinished = true;
      room.game.win = win;
    }

    this.sG.RoomS.updateGame(room, () => {
      //Nice :3
    }, () => {
      alert("Ocurrió un error al interta :C");
      this.c.listenerGame();
    })
  }

  /**
   *  -1 -> No one win and there's not a tie
   *  0-> Tie
   *  1-> win player A
   *  2-> win player B
   * @param board 
   * @returns 
   */
  private whoWinnerFinished(board: number[][], room: RoomI) {

    //Check each column
    for (let j = 0; j < 3; j++) {

      if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != 0) {
        return board[0][j];
      }

    }

    //Check each row
    for (let i = 0; i < 3; i++) {
      if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != 0) {
        return board[i][0];
      }
    }

    //Check the diagonals
    if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != 0) {
      return board[0][0];
    }
    if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] != 0) {
      return board[2][0];
    }


    if (room.game.movements == 9) {
      return 0;
    }

    return -1;

  }

}