import { Injectable } from '@angular/core';
import { RoomI, UpdateNamePlayerRequestI } from '../3.interfaces/models.interface';
import { RoomServiceI, ServiceResponse } from '../3.interfaces/services.interface';
import { AuthService } from './auth.service';

import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { map, Observable } from 'rxjs';

/**
 * The room about the game
 * 
 */
@Injectable({
  providedIn: 'root'
})
export class RoomService implements RoomServiceI {

  private roomItem: AngularFirestoreDocument<RoomI>

  private collection: AngularFirestoreCollection;

  constructor(public authS: AuthService, private afs: AngularFirestore) {


  }


  /**
   * Obtener juego actual con suscripción 
   * @param idRoom 
   * @returns 
   */
  getGame(idRoom: string) {
    return this.afs.collection<RoomI>("room").doc(idRoom).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data();
        return data
      }))
  }

  /**
   * Crea nuevo juego ;) xd 
   * @param room 
   * @param sv 
   */
  newGame(room: RoomI, successful: any, failure: any): void {

    const id = this.afs.createId();
    room.id = id;


    this.afs.collection<RoomI>(`room`).doc(id).set(room).then(_ => {
      successful(id);
    }).catch(e => {
      console.log("Ocurrió un error");
      failure("No se pudo crear la sala :C");
    })
  }




  /**
   * Player two joins the game
   * @param room 
   * @param successful 
   * @param failure 
   */
  joinGame(room: RoomI, successful: any, failure: any): void {

    this.afs.doc(`room/${room.id}`).update({
      playerB: room.playerB
    }).then(_ => {
      successful()
    }).catch(_ => {
      failure()
    })



  }



  /**
   * Update name of the user 
   * @param room 
   * @param nameNew 
   * @param sv 
   */
  updateNamePlayer(data: UpdateNamePlayerRequestI, successful: any, failure: any): void {

    let dat: any = {
      playerA: data.player

    }
    if (!data.isOwn) {
      dat = {
        playerB: data.player

      }
    }
    this.afs.doc(`room/${data.idRoom}`).update(dat).then(_ => {
      successful()
    }).catch(_ => {
      failure()
    })
  }

  /**
   * Restar the game
   * @param room 
   * @param sv 
   */
  restartingGame(room: RoomI, successful: any, failure: any): void {

    
    this.afs.doc(`room/${room.id}`).update({
      cont: room.cont,
      game: room.game
    }

    ).then(_ => {
      successful()
    }).catch(_ => {
      failure()
    })


  }

  /**
   * Actualiza el juego
   * @param room 
   * @param sv 
   */
  updateGame(room: RoomI, successful: any, failure: any): void {

    this.afs.doc(`room/${room.id}`).update({
      game: room.game
    }).then(_ => {
      successful()
    }).catch(_ => {
      failure()
    })
  }

}