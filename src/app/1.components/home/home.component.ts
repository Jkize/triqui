import { NgxSpinnerService } from 'ngx-spinner';
import { RoomI } from './../../3.interfaces/models.interface';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Base } from 'src/app/0.common/base.class';
import { ServicesGService } from 'src/app/2.services/services-g.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends Base implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder,
    private s: ServicesGService,
    public router: Router,
    private spinner: NgxSpinnerService
  ) {
    super()

    this.initForm()
  }

  ngOnInit(): void {
  }

  private initForm() {
    this.form = this.fb.group({
      'code': ['', Validators.required]
    })

  }



  /**
   * create a  New game 
   */
  newGame() {

    if (this.s.AuthS.isLogin()) {

      this.spinner.show()
      let auth = this.s.AuthS.getAuth();

      let room = {
        id: "",
        playerA: { id: auth.id, name: "Default player" },
        playerB: { id: "", name: "" },
        cont: 0,
        game: {
          board: { 0: [0, 0, 0], 1: [0, 0, 0], 2: [0, 0, 0] },
          isFinished: false,
          movements: 0
        }
      }

      this.s.RoomS.newGame(room, (dt: string) => {

        this.spinner.hide()
        this.router.navigateByUrl(`game/${dt}`)

      }, (s: string) => {
        this.spinner.hide()
        alert(s)

      })
    }
  }

  /**
   * Joint to the new game 
   */
  joinGame() {

    this.trimForm(this.form);
    if (this.form.valid) {
      this.router.navigateByUrl(`game/${this.form.get("code")?.value}`)
    }

  }

}
