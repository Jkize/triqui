import { GameComponent } from './../1.components/game/game.component';
import { HomeComponent } from './../1.components/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  { path: "home", component: HomeComponent },
  { path: "game/:id", component: GameComponent },
  { path: "**", pathMatch: "full", redirectTo: "home" }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
