# Triqui

Versión del angular: version 13.1.3 

## Correr proyecto

Tener instalado NodeJs  y ejecutar los siguientes comandos:
<br>
 1. "npm i" <br />
 2. "ng serve --open"     o el comando "npm start" <br />

## ¿Y ahora qué?
Disfrute del juego!! 

## NOTA
El juego funciona sin necesidad de 2 navegadores distintos ya que la información del usuario se guarda en sessionStorage
lo que permite jugar desde dos pestañas distintas en el mismo navegador o bueno también en diferentes navegadores.

