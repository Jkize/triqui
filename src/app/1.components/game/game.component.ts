import { ServicesGService } from './../../2.services/services-g.service';
import { Observable, Subscription } from 'rxjs';
import { Base } from 'src/app/0.common/base.class';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoomI, AuthI } from './../../3.interfaces/models.interface';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ControllerGameComponent } from './controller.game.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent extends Base implements OnInit, OnDestroy {

  controller: ControllerGameComponent;
  titleRest: String;

  room: RoomI = {
    id: "12345",
    playerA: { id: "112", name: "PlayerA" },
    playerB: { id: "112", name: "PlayerA" },
    cont: 0,
    game: {
      isFinished: false,
      board: { 0: [0, 2, 0], 1: [0, 1, 0], 2: [2, 2, 1] },
      movements: 5
    }
  }


  form: FormGroup;

  idSala = ""   // ID SALA :3 xd
  isOwn = false // Who create the room!!

  boardAux: number[][] = []

  suscribeGame: Subscription;

  constructor(
    private fb: FormBuilder,
    private route: Router,
    private sG: ServicesGService,
    private routeActive: ActivatedRoute) {
    super();
    this.controller = new ControllerGameComponent(this, sG);

    this.initForm()
  }


  private initForm() {
    this.form = this.fb.group({
      "name": ["", Validators.required]
    })
  }

  ngOnInit(): void {

    let a = (this.routeActive.snapshot.paramMap.get("id")) ? this.routeActive.snapshot.paramMap.get("id") : "";
    this.idSala = a!;
    if (a) {
      this.idSala = a;
      this.getUser();
    } else {
      this.route.navigateByUrl("");
    }


  }


  private user: AuthI

  /**
   * Obtener el usuario logueado, sí no está mandarlo al home
   */
  getUser() {

    if (this.sG.AuthS.isLogin()) {
      this.user = this.sG.AuthS.getAuth()
      this.listenerGame();
    } else {
      this.route.navigateByUrl("");
    }

  }

  /**
   *  The logical about join the game or check if the user already have loged before
   *  and the check if the user is the owner or not
   *  If a new user loggued to the room  -> update the room
   *  If there's two user in the room this redirects to the login
   * 
   *  
   */
  listenerGame() {
    this.suscribeGame = this.sG.RoomS.getGame(this.idSala).subscribe(r => {

      if (r != null) {
        this.room = r as RoomI;

        if (this.room.playerA.id == this.user.id) {
          this.isOwn = true;
          this.continueFlow();
        } else if (this.room.playerB.id == "") {
          //Set the player 
          this.room.playerB = { id: this.user.id, name: "" }
          this.sG.RoomS.joinGame(this.room, () => {
          },
            () => {
              alert("No se pudo unir al juego");
              this.route.navigateByUrl("");
            }
          )

        } else if (this.room.playerB.id != this.user.id) {
          //He/she is not in the game
          alert("Ya hay un jugador registrado en esta  sala");
          this.suscribeGame.unsubscribe();
          this.route.navigateByUrl("");
        } else {
          this.continueFlow();
        }
      } else {
        alert("Error, no se encontró la sala")
        this.route.navigateByUrl("");
      }

    }, err => {
      this.route.navigateByUrl("");
    })

  }

  ngOnDestroy(): void {
    this.suscribeGame.unsubscribe();
  }

  /**
   * Continue flow. There is a correct user for the game or the user already loged in the game 
   */
  private continueFlow() {

    this.form.get("name")?.setValue((this.isOwn) ? this.room.playerA.name : this.room.playerB?.name)
    this.boardAux = this.controller.getBoard(this.room);
  }

  /**
   * Action Update name
   */
  updateName(): void {

    this.trimForm(this.form);
    if (this.form.valid) {
      this.controller.updateName(this.isOwn, {
        id: this.user.id,
        name: this.form.get("name")?.value
      }, this.room);
    }
  }


  /**
   * Get image corresponding
   * @param num 
   * @returns 
   */
  getImage(num: number): String {

    if (num == 0) {
      return "/assets/blank.png"
    }

    if (num == 1) {

      return "/assets/X.png"

    } else {
      return "/assets/O.png"
    }

  }

  /**
   * Action click cell
   * @param i 
   * @param j 
   */
  clickCell(i: number, j: number) {

    if (!this.room.game.isFinished && this.boardAux[i][j] == 0 && this.controller.isMyTurn(this.isOwn, this.room)) {
      this.controller.continueClickCell(this.isOwn, i, j, this.room, this.boardAux);
    }

  }

  /**
   * Action restar game
   */
  restartGame() {
    this.controller.restarGame(  this.room);
  }

}



